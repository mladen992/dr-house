import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientsRoutingModule } from './patients-routing.module';
import { PatientsAllComponent } from './pages/patients-all/patients-all.component';
import { PatientsEditComponent } from './pages/patients-edit/patients-edit.component';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { NgxMaskModule } from 'ngx-mask';



@NgModule({
  declarations: [PatientsAllComponent, PatientsEditComponent],
  imports: [
    CommonModule,
    PatientsRoutingModule,
    SharedModule,
    RxReactiveFormsModule,
    NgxMaskModule.forRoot(),
  ]
})
export class PatientsModule { }
