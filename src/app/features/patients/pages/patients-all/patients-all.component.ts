import { PatientsService } from './../../../../shared/services/patients.service';
import { Patient } from './../../../../shared/models/patients/patient-model';
import { Doctor } from './../../../../shared/models/doctors/doctor-model';
import { DoctorsService } from './../../../../shared/services/doctors.service';
import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { Router } from '@angular/router';
import { NotificationsService } from 'src/app/shared/services/notifications.service';

@Component({
  selector: 'app-patients-all',
  templateUrl: './patients-all.component.html',
  styles: [
  ]
})
export class PatientsAllComponent implements OnInit {

  patients!: Patient[];
  currentPage: number = 1;

  constructor(
    private _doctorsService: DoctorsService,
    private _patientsService: PatientsService,
    private _router: Router,
    private _notificationService: NotificationsService
    ) { }

  ngOnInit(): void {
    this.loadData();
  }

  loadData(): void{
    forkJoin([
      this._doctorsService.getAll(),
      this._patientsService.getAll(),
    ]).subscribe(([doctors, patients]) => {
      this.patients = this.mapDoctorsToPatients(patients, doctors)
      console.log(patients);
    });
  }

  mapDoctorsToPatients(patients: Patient[], doctores: Doctor[]): Patient[] {
    patients.forEach(patient => {
      patient.doctorDetails = doctores.find(x => x.id === patient.doctor);
    })
    return patients;
  }

  mailto(mail: string){
    window.location.href = `mailto:${mail}?subject=test&body=test`;
  }

  onEdit(id: number) {
    this._router.navigate([`/patients/${id}`]);
  }

  copyPhone(data: string){
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = '+39' + data;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this._notificationService.success('Copy', 'Phone is copied to clipboard');
  }

  onPageChange(page: number) {
    this.currentPage = page;
  }

}
