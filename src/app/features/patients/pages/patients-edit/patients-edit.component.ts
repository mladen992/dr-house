import { Doctor } from './../../../../shared/models/doctors/doctor-model';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Patient } from 'src/app/shared/models/patients/patient-model';
import { DoctorsService } from 'src/app/shared/services/doctors.service';
import { PatientAddressType } from 'src/app/shared/models/patients/patient-address-type-enum';
import { PatientAddress } from 'src/app/shared/models/patients/patient-address-model';
import { RxwebValidators } from '@rxweb/reactive-form-validators';
import { NotificationsService } from 'src/app/shared/services/notifications.service';
import { PatientsService } from 'src/app/shared/services/patients.service';

@Component({
  selector: 'app-patients-edit',
  templateUrl: './patients-edit.component.html',
  styles: [
  ]
})
export class PatientsEditComponent implements OnInit {

  form!: FormGroup;
  patient!: Patient;
  focused!: string;
  doctors!: Doctor[];
  addressType = Object.values(PatientAddressType);

  constructor(
    private _fb: FormBuilder,
    private _router: Router,
    private _route: ActivatedRoute,
    private _doctorsService: DoctorsService,
    private _patientsService: PatientsService,
    private _notificationService: NotificationsService
  ) { }

  ngOnInit(): void {
    this.loadDoctors();
    this.loadData()
  }

  loadDoctors(){
    this._doctorsService.getAll().subscribe(res => {
      this.doctors = res;
    })
  }

  loadData() {
    let patientsId = this._route.snapshot.params.id;

    if (patientsId) {
      this._patientsService.getOne(patientsId).subscribe((res) => {
        this.patient = res;
        console.log(this.patient);
        this.initForm();
      })
    } else {
      this.patient = {} as Patient;
      this.patient.addresses = [];
      this.initForm();
    }
  }

  initForm() {
    this.form = this._fb.group({
      id: [this.patient.id || null],
      registeredDate: [this.patient.registeredDate || new Date()],
      firstName: [this.patient.firstName || "", [Validators.required, Validators.maxLength(35), Validators.minLength(2)]],
      lastName: [this.patient.lastName || "", [Validators.required, Validators.maxLength(35), Validators.minLength(2)]],
      doctor: [this.patient.doctor || null, [Validators.required]],
      birthDate: [this.patient.birthDate || null, [Validators.required]],
      vatCode: [this.patient.vatCode || null, [RxwebValidators.required({conditionalExpression:(x: any)=> x.birthDate && this.validatorOver18(x.birthDate)})]],
      addresses: this._fb.array(this.buildFAAddress(this.patient.addresses))
    });
  }

  buildFAAddress(addresses: PatientAddress[]): FormGroup[] {
    let fgArray: FormGroup[] = [];
    if(addresses.length > 0){
      addresses.forEach(a => fgArray.push(this.buildAddress(a)))
    }else {
      fgArray.push(this.buildAddress())
    }
    return fgArray;
  }

  buildAddress(address: PatientAddress = {} as PatientAddress): FormGroup {
    return this._fb.group({
      type: [address.type || null, Validators.required],
      email: [address.email || '', [Validators.required, Validators.email]],
      phone: [address.phone || '', Validators.required],
      street: [address.phone || '', Validators.required],
      city: [address.city ||'', Validators.required],
      zipcode: [address.zipcode || '', Validators.required],
      country: [address.country || '', Validators.required],
    });
  }

  // Actions
  save() {
    if (!this.formValid()) return;
    let results = Object.assign({}, this.form.getRawValue());
    if(results.id){
      this._patientsService.update(results).subscribe(()=> {
        this._notificationService.success(
          "Updated",
          "Patient updated"
        );
        this._router.navigate(["/patients"]);
      })
    }
    this._patientsService.create(results).subscribe(()=> {
      this._notificationService.success(
        "Created",
        "Patient created"
      );
      this._router.navigate(["/patients"]);
    })
  }

  addAddress(): void {
    this.addresses.push(this.buildAddress());
  }
  
  // Other
  formValid(): boolean {
    if (!this.form.valid && !this.form.disabled) {
      this._notificationService.error(
        "Form not valid",
        "Please fill out all highlighted fields."
      );
      this.form.markAllAsTouched();
      this.focusInvalidInput();
      return false;
    } else return true;
  }

  focusInvalidInput() {
    setTimeout(() => {
      document
        .getElementsByClassName("error")[0]
        .scrollIntoView({ behavior: "smooth", block: "center" });
    }, 100);
  }

  validatorOver18(birthDate: Date){
    if(!birthDate){
      return false;
    }
    let today = new Date();
    var age = today.getFullYear() - birthDate.getFullYear();
    if (age < 18){
      return false;
    }
    return true;
  }

  get addresses(): FormArray {
    return <FormArray>this.form.get('addresses');
  }

  get f() {
    return this.form.controls;
  }


}
