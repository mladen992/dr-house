import { PatientsEditComponent } from './pages/patients-edit/patients-edit.component';
import { PatientsAllComponent } from './pages/patients-all/patients-all.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: "",
        redirectTo: "all",
        pathMatch: 'full',
      },
      {
        path: 'all',
        children: [
          {
            path: '',
            component: PatientsAllComponent,
          },
          {
            path: ':id',
            component: PatientsEditComponent,
          },
        ],
      },
      {
        path: 'new',
        component: PatientsEditComponent,
      },
      {
        path: ':id',
        component: PatientsEditComponent,
      }
      
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientsRoutingModule { }
