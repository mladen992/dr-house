import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styles: [
  ]
})
export class NavigationComponent implements OnInit {

  navigationVisible: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  toggleNavigation(){
    this.navigationVisible = !this.navigationVisible;
  }

}
