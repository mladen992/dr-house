import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path : '', loadChildren : () => import('./features/home/home.module').then(m => m.HomeModule), data: { title: 'Home' } },
  { path : 'patients', loadChildren : () => import('./features/patients/patients.module').then(m => m.PatientsModule), data: { title: 'Patients' } },
  { path: "**", redirectTo: ""},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
