import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { NotificationsService } from '../../services/notifications.service';
import { NotificationType, Notification } from '../../models/notifications/notification';

@Component({
  selector: 'notifications',
  templateUrl: './notifications.component.html',
  styles: [
  ]
})
export class NotificationsComponent implements OnInit {

  constructor(
    private _notificationService: NotificationsService, ) { }
  
  notifications: Notification[] = [];
  private _subscription = new Subscription();

  ngOnInit() {
    this._subscription.add(this._notificationService.getNotifications().subscribe(notification=>{
      this.addNotification(notification)
    }))
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

  private addNotification(notification: Notification){
    this.notifications.push(notification);

    if (notification.timeout !== 0) {
      setTimeout(() => this.close(notification), notification.timeout);
    }
  }

  close(notification: Notification) {
    this.notifications = this.notifications.filter(notif => notif.id !== notification.id);
  }

}
