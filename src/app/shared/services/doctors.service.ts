import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Doctor } from '../models/doctors/doctor-model';

@Injectable({
  providedIn: 'root'
})
export class DoctorsService {

  constructor(private http: HttpClient) {}

  private resource = `${environment.server}/doctors/`;

  getAll(): Observable<Doctor[]> {
    return this.http.get<Doctor[]>(this.resource);
  }
}
