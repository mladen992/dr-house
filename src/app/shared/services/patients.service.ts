import { Patient } from './../models/patients/patient-model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PatientsService {

  constructor(private http: HttpClient) {}

  private resource = `${environment.server}/patients/`;

  getAll(): Observable<Patient[]> {
    return this.http.get<Patient[]>(this.resource);
  }

  getOne(id: number): Observable<Patient> {
    return this.http.get<Patient>(this.resource + id);
  }

  create(patient: Patient): Observable<Patient>{
    return this.http.post<Patient>(this.resource, patient);
  }

  update(patient: Patient): Observable<Patient>{
    return this.http.put<Patient>(`${this.resource}${patient.id}`, patient);
  }
}
