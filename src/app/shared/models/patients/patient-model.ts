import { Doctor } from './../doctors/doctor-model';
import { PatientAddress } from "./patient-address-model";

export interface Patient {
    id: number;
    registeredDate: Date;
    firstName: string;
    lastName: string;
    doctor:  number;
    addresses: PatientAddress[];
    doctorDetails?: Doctor;
    birthDate?: Date;
    vatCode?: number;
}


