
export enum PatientAddressType {
    HOME = "HOME",
    SecondHome = "Second Home",
    Work = "Work",
    HolidayPlace = "Holiday place",
    CloseRelative = "Close relative",
}
