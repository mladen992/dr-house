import { PatientAddressType } from './patient-address-type-enum';

export interface PatientAddress {
    type: PatientAddressType;
    email: string;
    phone: string;
    street:  string;
    city: string;
    zipcode: string;
    country: string
}


