export enum DoctorTitle {
    Immunologist = "Immunologist",
    Surgeon = "Surgeon",
    MD = "MD",
  }