import { DoctorTitle } from './doctor-title-enum';

export interface Doctor {
    id: number;
    firstName: string;
    lastName: string;
    title: DoctorTitle;
}


